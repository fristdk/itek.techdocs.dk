# Welcome to itek.techdocs.dk

![BAAA_logo](/img/itek1/BAA_RGB.png)
A warm welcome here to this home of guides and technical documents introducing you to various technologies during your education as an IT-Technologist at Business Academy Aarhus (BAAA).


!!! note "What is..?"
    What is a technologist, anyway? In the broadest sense, a technologist is a Technology Specialist, one who specializes in technology and works with modern technology, especially technology relating to a particular activity or industry.


## How to navigate
On the left you will find the different semesters and under each you will find more detailed guids related to specific topics that you are working with during your education.

## I’m a person that links to read things on paper
You are lucky, so am I. At the bottom of the screen you will find an option for downloading a pdf version of this site if you need it in that format. From here you are allowed to print it as much as you like but just do not waste copies and kill the forest. 
